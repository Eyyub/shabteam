{shared{
  open Eliom_lib
  open Eliom_content
  open Html5.D
}}

module Shabteam_app =
  Eliom_registration.App (
    struct
      let application_name = "shabteam"
    end)

let main_service =
  Eliom_service.App.service ~path:[] ~get_params:Eliom_parameter.unit ()

let reactdraw_service =
  Eliom_service.App.service ~path:["reactdraw"] ~get_params:Eliom_parameter.unit ()

let () =
  Shabteam_app.register
    ~service:main_service
    (fun () () ->
      Lwt.return
        (Eliom_tools.F.html
           ~title:"Shab Team"
           ~css:[["css";"shabteam.css"]; ["css"; "animate.css"]]
           Html5.F.(body [
	div [
             img ~alt:"Shab Team logo" ~src:(Xml.uri_of_string "https://pbs.twimg.com/profile_images/532221167709339648/hoYAmyc5.png") ();
	     br (); br (); br (); br ();
	     h1 ~a:[a_style "color: #ffffff"; a_class ["animated flash shabElem divElementCenter"]] [pcdata "Shab Team, coming soon..."];
           ]])));
  Shabteam_app.register
    ~service:reactdraw_service
    (fun () () ->
       let _ = {unit{ Reactdraw.init_client () }} in
       Lwt.return
         (Eliom_tools.F.html
           ~title:"Reactdraw"
           ~css:[["css";"reactdraw.css"]]
           Html5.F.(body [])))
